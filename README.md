This code is part of the published paper: "Gutiérrez, Y., Arevalo, J., & Martínez, F. (2020, January). A Ktrans deep characterization to measure clinical significance regions on prostate cancer. In 15th International Symposium on Medical Information Processing and Analysis (Vol. 11330, p. 113300C). International Society for Optics and Photonics."

Please cite our paper if you use the code.

---
`` @inproceedings{gutierrez2020ktrans,
  title={A Ktrans deep characterization to measure clinical significance regions on prostate cancer},
  author={Guti{\'e}rrez, Yesid and Arevalo, John and Mart{\'\i}nez, Fabio},
  booktitle={15th International Symposium on Medical Information Processing and Analysis},
  volume={11330},
  pages={113300C},
  year={2020},
  organization={International Society for Optics and Photonics}
} ``
---


## Prostate cancer lesions classification
 **Yesid Gutiérrez, John Arevalo, Fabio Martínez**
### Universidad Industrial de Santander (UIS). Colombia

Nueva linea aqui
Se agrega otra linea.



